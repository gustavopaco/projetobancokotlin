package br.com.cotemig.bancokotlin.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.cotemig.bancokotlin.R
import br.com.cotemig.bancokotlin.models.ContaCorrente
import kotlinx.android.synthetic.main.fragment_detalhe.view.*

class DetalheFragment : Fragment() {

    lateinit var conta : ContaCorrente

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        val view =  inflater.inflate(R.layout.fragment_detalhe, container, false)


        view.titular.text = conta.nome
        view.nconta.text = conta.numeroConta.toString()
        view.saldo.text = conta.saldo.toString()


        return view
    }

    companion object {

        fun newInstance(conta : ContaCorrente) : DetalheFragment{
            var f = DetalheFragment()
            f.conta = conta
            return f
        }

    }
}