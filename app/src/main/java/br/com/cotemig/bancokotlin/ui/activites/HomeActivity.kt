package br.com.cotemig.bancokotlin.ui.activites

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import br.com.cotemig.bancokotlin.R
import br.com.cotemig.bancokotlin.models.ContaCorrente
import br.com.cotemig.bancokotlin.ui.fragments.DepositoFragment
import br.com.cotemig.bancokotlin.ui.fragments.DetalheFragment
import br.com.cotemig.bancokotlin.ui.fragments.SaqueFragment
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    var conta = ContaCorrente()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        //carregar objetos da tela com dados da conta.
        conta.numeroConta = intent.getStringExtra("numeroConta")!!.toInt()
        conta.nome = intent.getStringExtra("titular").toString()

        setFragment(DetalheFragment.newInstance(conta), "detalhe")



        btndetalhes.setOnClickListener {
            menu(0)
        }
        btnsaque.setOnClickListener {
            menu(1)
        }

        btndeposito.setOnClickListener {
            menu(2)
        }

    }

    fun setFragment(f: Fragment, name: String) {
        val ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.contenthome, f, name)
//        ft.addToBackStack(name)
        ft.commit()
    }

    fun menu(index: Int) {
        if (index == 0) {
            setFragment(DetalheFragment.newInstance(conta), "detalhe")
        } else if (index == 1) {
            setFragment(SaqueFragment.newInstance(conta), "saque")
        } else if (index == 2) {
            setFragment(DepositoFragment.newInstance(conta),"deposito")
        }
    }



}