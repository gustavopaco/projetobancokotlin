package br.com.cotemig.bancokotlin.models

data class ContaCorrente (
    var nome : String = "",
    var numeroConta : Int = 0,
    var saldo : Double = 0.00,
)