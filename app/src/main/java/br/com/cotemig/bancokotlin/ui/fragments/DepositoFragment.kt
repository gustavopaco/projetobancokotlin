package br.com.cotemig.bancokotlin.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.cotemig.bancokotlin.R
import br.com.cotemig.bancokotlin.models.ContaCorrente
import br.com.cotemig.bancokotlin.ui.activites.HomeActivity
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.Theme
import kotlinx.android.synthetic.main.fragment_deposito.view.*

class DepositoFragment : Fragment() {

    lateinit var conta : ContaCorrente

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_deposito, container, false)

        view.saldo_disponivel.text = conta.saldo.toString()

        view.btnconfirmardeposito.setOnClickListener {
            validacampo()
        }


        return view
    }

    companion object {
            fun newInstance(conta : ContaCorrente) : DepositoFragment{
                var f = DepositoFragment()
                f.conta = conta
                return f
            }
    }


    fun validacampo(){

        view?.let{ v ->

            if(v.deposito.text.isEmpty()){
                MaterialDialog.Builder(context as HomeActivity).theme(Theme.LIGHT)
                    .title(R.string.error)
                    .content(getString(R.string.informedeposito)).positiveText(R.string.ok).show()
            }else
                depositar()

        }
    }

    fun depositar(){
        conta.saldo = view!!.deposito.text.toString().toDouble() + conta.saldo
        view!!.saldo_disponivel.text = conta.saldo.toString()
        view!!.deposito.text.clear()
    }
}