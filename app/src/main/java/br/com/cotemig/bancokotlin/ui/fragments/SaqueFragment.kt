package br.com.cotemig.bancokotlin.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.cotemig.bancokotlin.R
import br.com.cotemig.bancokotlin.models.ContaCorrente
import br.com.cotemig.bancokotlin.ui.activites.HomeActivity
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.Theme
import kotlinx.android.synthetic.main.fragment_saque.view.*

class SaqueFragment : Fragment() {

    lateinit var conta : ContaCorrente

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_saque, container, false)

        view.saldo_disponivel.text = conta.saldo.toString()

        view.btnconfirmarsaque.setOnClickListener {
            validacampo()
        }


        return view
    }

    companion object {
        fun newInstance(conta: ContaCorrente): SaqueFragment {
            var f = SaqueFragment()
            f.conta = conta

            return f
        }
    }

    fun validacampo() {

        view?.let { v ->

            if (v.saque.text.isEmpty()) {
                MaterialDialog.Builder(context as HomeActivity).theme(Theme.LIGHT)
                    .title(R.string.error)
                    .content(R.string.informesaque).positiveText(R.string.ok).show()
            } else
                sacar()

        }
    }

    fun sacar() {
        view?.let { v ->

            var s = v.saque.text.toString().toDouble()
            if (s <= conta.saldo) {
                conta.saldo = conta.saldo - s
                v.saldo_disponivel.text = conta.saldo.toString()
                v.saque.text.clear()
            } else
                MaterialDialog.Builder(context as HomeActivity).theme(Theme.LIGHT)
                    .title(R.string.error)
                    .content("Saldo insuficiente").positiveText(R.string.ok).show()

        }
    }

}