package br.com.cotemig.bancokotlin.ui.activites

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import br.com.cotemig.bancokotlin.R
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.Theme
import kotlinx.android.synthetic.main.activity_entrada.*

class EntradaActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_entrada)

        btn_enviar.setOnClickListener {
            validacampo()
        }

    }

    fun validacampo() {
        if (numerodaconta.text.isEmpty() || titularEntrada.text.isEmpty()) {
            MaterialDialog.Builder(this).theme(Theme.LIGHT).title(getString(R.string.error))
                .content(getString(R.string.preencha)).positiveText(
                    getString(
                        R.string.ok
                    )
                ).show()
        } else
            goDetalheActivity()
    }

    fun goDetalheActivity() {
        var intent = Intent(this, HomeActivity::class.java)
        intent.putExtra("numeroConta", numerodaconta.text.toString())
        intent.putExtra("titular", titularEntrada.text.toString())
        startActivity(intent)
        finish()
    }

}